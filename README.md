# Laboratorio Redes ? Comandos De Red  #
       # Mateo Montoya Pachon, Andres Felipe Jimenez#

parte #1 

ipconfig : este comando se utiliza para obtener los datos de la red en la cual está alojado el dispositivo. 

ping: este comando se utiliza para hacer un diagnóstico de estado de un host ya sea local o remoto. 

Tracert : este comando permite ver la ruta tomada de un paquete enviado 

nestat : Este comando muestra las conexiones TCP activas, los puertos usados por la computadora, las estadísticas de Internet y la tabla de enrutamiento de IP. 

pathping : Es un comando parecido a Tracert el cual permite ver los enrutadores que están en las rutas de acceso. 

Telnet: permite abrir comunicación con un equipo remoto que ejecute el servicio de telnet 

Net: es un comando que se usa para establecer la configuración en el equipo local 

Route: Este comando manipula las tablas de enrutamiento de red 

Arp: Este comando muestra y modifica las tablas de conversión de direcciones IP a direcciones físicas. 

Nbstat: muestra tanto las estadísticas de protocolo NetBios como las tablas de nombres NetBios para la computadora local 

Netsh: Permite mostrar o modificar la configuración de red de una computadora en ejecución. 

GetMac : obtiene las direcciones Mac que tienen asociados los adaptadores de red. 

Parte #2 

 a ) Un administrador de red digital comando de red mostrado en la Figura 1. ¿Qué significa la dirección de red que se encuentra entre los corchetes? 

 La dirección de red que se encuentra entre los corchetes hace referencia a la dirección, en este caso de Google. Mediante el sistema de nombres de dominio (DNS), la dirección de red se traduce con un nombre de dominio para que se búsqueda sea mas sencilla. Así pues se puede hacer ping con la dirección ip o con el nombre de dominio. 

 b) Un administrador de red digita el comando de red mostrado en la Figura 2. ¿Qué quiere decir Tiempo de espera agotado para esta solicitud? 

 cuando el administrador utilizo nuevamente el comando ping el dispositivo arrojo tiempo de espera agotado lo cual quiere decir que no hay conectividad entre el dispositivo y la dirección IP. Y por esto los paquetes se enviaron, pero no se recibieron.

 c) Un administrador de red digita el comando de red mostrado en la Figura 3. Explique el significado de: (1) bytes=32, (2) tiempo=7ms, y (3) TTL=64. 

 El administrador al usar nuevamente el comando ping sobre el dispositivo puede evidenciar que su paquete tiene un tamaño de 32 bytes, un tiempo de respuesta de 7 milisegundos y un tiempo de vida (TTL) del paquete enviado con un valor máximo de 64 

 d) Un administrador de red digita el comando de red mostrado en la Figura 4. ¿Qué direcciones IP y físicas se muestran en la figura? ¿Qué significa direccionamiento dinámico? 

 Tras usar el comando arp -g se pueden evidenciar dos direcciones IP y dos direcciones físicas 

 IP: 192.168.1.1 // 192.168.1.229 

 Físicas: 00-0c-f1-86-5e-h9 // 00-80-ad-04-4e-29 

 Se utiliza el protocolo de configuración dinámica de host (DHCP) el cual asigna una dirección dinámica al dispositivo que se conecta a esa red. El direccionamiento dinámico significa que al usar el dispositivo conectado a la red cada vez que se conecte puede que esta dirección ip cambie ya que no es estática por lo cual es una dirección que cambia cada cierto tiempo esto significa un direccionamiento dinámico 

 e) Un administrador de red digita el comando de red mostrado en la Figura 5. Explique los términos Destino de red, Máscara de red, Puerta de acceso, Interfaz, Métrica 

 El comando netstat -r muestra la tabla de ruta que existen en la red. El enrutamiento que tiene la red.
 
 Mascara de Red: Combinación de bits para delimitarla la red de hosts.

 Destino Red:Indica la red con la que hay posibilidad de conexión. El destino de red se utiliza con la máscara de red para coincidir con la dirección IP de destino.

 Puerta de acceso: Indica la dirección que el host utiliza para enviar los paquetes a un destino de red remoto.
 
 Interfaz: Indica la dirección IP que está configurada en el adaptador de red local. Se utiliza para reenviar un paquete en la red.

 Métrica: Indica el costo de usar una ruta. Calcula la mejor ruta a un destino. 

 f) Un administrador de red digita el comando de red mostrado en la Figura 6. ¿Para qué sirve esa información? ¿Realiza la misma función de netstat –p TCP? 

 El comando utilizado por el administrador el cual es nestat/ lo que permite es ver todas las conexiones activas de un host.
 
 En este caso, hay varias conexiones con el protocolo de control de transmisión/Protocolo de Internet (TCP/IP) Y nos muestra datos del estado de conexión, si se estableció, si esta en espera o no se pudo establecer. Además la dirección local y remota de la conexión.
  
 La función netstat -p filtra el tipo de protocolo el cual va a realizar la búsqueda, al usar nestat -p TCP lo que permite es ver solo las estadísticas de los protocolos TCP/IP. 

 g) Un administrador de red digita el comando de red mostrado en la Figura 7. ¿Qué significa Traza? ¿Qué indica el número de saltos? ¿Qué significa el <1 ms en cada salto? ¿Para qué se realiza una Traza? ¿En qué momento es útil utilizar el comando Tracert? 

 Traza es la captura del tráfico de red que se envía y recibe entre diferentes puntos de red 

 El número de saltos es el número de veces que se pasa un paquete de un segmento de red al siguiente en este caso hubo 10 saltos o 10 veces que se pasó el paquete entre segmentos de red 

 1ms en cada salto fue lo que tardo el paquete de pasar de un segmento de red al siguiente 

 La traza fue utilizada con el fin de ver la captura de trafico de la red por las cuales paso el paquete 
 
 El comando tracert se utiliza para ver la ruta que tomo el paquete después de enviado. 

 

 

Parte #3 

	Consulte y describa brevemente los comandos de red mas utilizados en la administración y diagnostico de redes en ambientes Linux.  

 

1) comando ping al ejecutar el comando este envía paquetes “Echo_Request” con el propósito de ver si el computador o dispositivo se puede comunicar con un dirección IP. 

2) El comando Tracepath es el comando el cual traza la ruta de red a cualquier lugar o IP especificada con el fin de ver los saltos en cada punto. 

3) Comando mtr es un comando el cual envía paquetes y muestra el ping en cada salto de red que haya.  

4) Comando ifconfig este comando permite configurar la interfaz de red aparte de que tambien muestra todas las direcciones IP y más informacion sobre la red. 

5) ifconfig up / down permites habilitar o deshabilitar una interfaz de red seleccionada. 

6) Host es un comando el cual realiza una búsqueda DNS  y permitirá ver la direcccion IP asociada al dispositivo. 

7) Route este comando permite ver y modificar la tabla de enrutamiento. 

8) Curl este comando permite descargar un archivo de internet sin salir de la consola. 

9) Whois permite ver la informacion de la persona que se registro su informacion, y quien es el propietario de un sitio web en especifico. 

 

 

Bibliografia: 

 

1) ¿Qué ES EL ARP (Address resolution protocol)? IONOS Digitalguide. (n.d.). https://www.ionos.es/digitalguide/servidores/know-how/arp-resolucion-de-direcciones-en-la-red/. 

2) 3.3.5. red de destino - curso en linea ccna1. Google Sites. (n.d.). https://sites.google.com/site/cursoenlineaccna1/unidad-3-capa-de-red-osi/3-3-enrutamiento-como-se-manejan-nuestros-paquetes-de-datos/3-3-5-red-de-destino. 

3) 100001543332907. (2019, October 10). Como cambiar LA DIRECCIÓN Ip DE estática A Dinámica y viceversa. NorfiPC. https://norfipc.com/redes/cambiar-direccion-ip-dinamica-estatica.html. 

4) ¿Qué ES una máscara De red? Speedcheck. (n.d.). https://www.speedcheck.org/es/wiki/mascara-de-red/. 

5) Direcciones IP, MÁSCARAS de subred Y puertas De acceso. (n.d.). https://support.brother.com/g/s/id/htmldoc/printer/cv_hl4040cn/spa/html/spa/nug/chapter2_2.html. 

6) FireWare Help. Acerca de los Modos de red Y las interfaces. (n.d.). https://www.watchguard.com/help/docs/fireware/12/es-419/Content/es-419/networksetup/net_setup_about_c.html. 

7) Deland-Han. (n.d.). La Característica Métrica automática para rutas ipv4 - windows server. La característica Métrica automática para rutas IPv4 - Windows Server | Microsoft Docs. https://docs.microsoft.com/es-es/troubleshoot/windows-server/networking/automatic-metric-for-ipv4-routes#:~:text=Una%20métrica%20es%20un%20valor,saltos%20o%20retraso%20de%20tiempo. 

8) ¿Qué es netstat y cómo funciona? IONOS Digitalguide. (n.d.). https://www.ionos.es/digitalguide/servidores/herramientas/una-introduccion-a-netstat/. 

9) Cómo sacar una traza y por qué es tan importante. Sinologic.net :: La web más leída sobre VoIP en Español. (2014, February 11). https://www.sinologic.net/2014-02/como-sacar-una-traza-y-por-que-es-tan-importante.html#:~:text=Una%20traza%20es%20una%20captura,el%20dispositivo%20que%20estamos%20estudiando. 

10) Comportamientos por Salto (Guía de Administración del Sistema: Servicios IP). (n.d.). https://docs.oracle.com/cd/E19957-01/820-2981/ipqos-intro-10/index.html. 

11) Comando Path Ping. EcuRed. (n.d.). https://www.ecured.cu/Comando_Path_ping#:~:text=Pathping%20realiza%20el%20equivalente%20del,número%20que%20devuelve%20cada%20uno. 

12) Deland-Han. (n.d.). Comandos de Red Para Sistemas operativos - windows server. Comandos de red para sistemas operativos - Windows Server | Microsoft Docs. https://docs.microsoft.com/es-es/troubleshoot/windows-server/networking/net-commands-on-operating-systems#:~:text=NET,-CUENTAS&text=El%20comando%20"Cuentas%20netas"%20se,cuenta%20y%20directivas%20de%20contraseña.&text=Si%20el%20equipo%20está%20unido,la%20configuración%20procedente%20del%20dominio. 

13) Pandora FMS teamEl equipo de redacción de Pandora FMS está formado por un conjunto de escritores y profesionales de las TI con una cosa en común: su pasión por la monitorización de sistemas informáticos.Pandora FMS’s editorial team is made up of a group o. (2021, June 28). Comandos de red: ¿sabes ya qué debes conocer en 2020? Pandora FMS - The Monitoring Blog. https://pandorafms.com/blog/es/comandos-de-red/. 

14) Moreno, J. A. M. (2020, November 13). 20 Comandos de Red Más importantes en windows. OpenWebinars.net. https://openwebinars.net/blog/20-comandos-de-red-mas-importantes-en-windows/. 

15) lectura, L. M. de, también, V., Linux, & Alexynior·. (2017, August 23). 10+ Comandos de Red Para La Terminal Linux. EsGeeks. https://esgeeks.com/10-comandos-de-red-para-la-terminal-linux/. 

16) Visualizacion de tablas de enrutamiento (Cisco) https://static-course-assets.s3.amazonaws.com/ITN50ES/files/6.2.2.8%20Lab%20-%20Viewing%20Host%20Routing%20Tables.pdf